@extends('layouts.admin.admin')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Tickets</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('ticket.index')}}">Ticket</a></li>
              <li class="breadcrumb-item active">Listes</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
        <form method="post" action="{{route('filter.ticket')}}">
          {{csrf_field()}}
          <div class="row">
            <div class="col-sm-2">
              <input type="text" name="search"  placeholder="Rechercher" value="" class="form-control">
            </div>
            <div class="col-sm-2">
              <select class="form-control select2 select2-hidden-accessible" style="width: 100%;"  tabindex="-1" aria-hidden="true" name="status">
                {{-- @foreach($packages as $k => $v)
                  <option value="{{$v->id}}">{{$v->name}}</option>
                @endforeach --}}
                <option value="">Choisir un status</option>
                <option value="2">En entente</option>
                <option value="0">Non vendu</option>
                <option value="1">Vendu</option>
              </select>
            </div>
            <div class="col-sm-2">
              <button type="submit" class="btn btn-success btn-md mb-2">Valider</button>
            </div>
          </div>
        </form>
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
            <button type="button" class="btn btn-primary btn-lg float-sm-right mb-2" href="{{route('ticket.create')}}" data-toggle="modal" data-target="#modal-sm" >Ajouter des tickets</a>
          </div>

          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Listes de vos tickets</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">Id</th>
                      <th>Nom d'utilisateur</th>
                      <th>Mot de passe</th>
                      <th>Prix</th>
                      <th>Etat</th>
                      <th style="width: 40px">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($tickets as $k => $v)
                    <tr>
                      <td>{{$k+1}}</td>
                      <td>{{$v->name}}</td>
                      <td>{{$v->password}}</td>
                      <td>{{$v->price}}</td>
                      <td>
                        @if ($v->status == 0) 
                        <span class="right badge badge-success">Valide</span> 
                        @elseif ($v->status == 1)
                        <span class="right badge badge-danger">Vendu</span> 
                        @elseif ($v->status == 2)
                        <span class="right badge badge-warning">Entente</span>
                        @endif
                      </td>
                      <td>
                        <a class="btn btn-primary" href="{{route('ticket.edit',$v)}}">Invalider</a>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="d-flex justify-content-center">
                {!! $tickets->links() !!}
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->

    <div class="modal fade" id="modal-sm" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-sm">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Choisir votre fichier</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <form method="post" action="{{ route('ticket.importcsv') }}" enctype="multipart/form-data">
              {{csrf_field()}}
              <div class="modal-body">
                <div class="form-group">
                  <label for="csvFile">Fichier CSV</label>
                  <div class="input-group">
                    <div class="custom-file">
                      <input type="file" name="csvFile" class="custom-file-input" id="csvFile" accept=".csv">
                      <label class="custom-file-label" for="csvFile">Choisir csv</label>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                    <label>Forfait</label>
                    <select class="form-control select2 select2-hidden-accessible" style="width: 100%;"  tabindex="-1" aria-hidden="true" name="package_id">
                      @foreach($packages as $k => $v)
                        <option value="{{$v->id}}">{{$v->name}}</option>
                      @endforeach
                    </select>
                  </div>
              </div>
              <div class="form-group">
                    <label>Colonne d'identifiant</label>
                    <select class="form-control select2 select2-hidden-accessible" style="width: 100%;"  tabindex="-1" aria-hidden="true" name="id">
                      <option value="1" selected>1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                    </select>
                  </div>
              </div>
              <div class="form-group">
                    <label>Colonne de mot de passe</label>
                    <select class="form-control select2 select2-hidden-accessible" style="width: 100%;"  tabindex="-1" aria-hidden="true" name="password">
                      <option value="1">1</option>
                      <option value="2" selected>2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                    </select>
                  </div>
              </div>
              <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                <button type="submit" class="btn btn-primary">Envoyer</button>
              </div>
            </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
  </div>
@endsection

