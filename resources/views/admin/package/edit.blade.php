@extends('layouts.admin.admin')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Ajouter un forfait</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('package.index')}}">Forfait</a></li>
              <li class="breadcrumb-item active">Ajouter un forfait</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-md-12">
            <div class="card card-primary">
              <!-- /.card-header -->
              <!-- form start -->
              <form method="post" action="{{ route('package.update', $package) }}">
                {{csrf_field()}}
                <input type="hidden" name="_method" value="PUT">
                <div class="card-body"> 
                  <div class="form-group">
                    <label for="packagename">Nom du Forfait</label>
                    <input type="text" class="form-control" name="packagename" id="packagename" placeholder="Nom du Forfait" value="{{$package->name}}" >
                  </div>
                  <div class="form-group">
                    <label for="price">Prix</label>
                    <input type="text" class="form-control" id="price" name="price" placeholder="Prix: 100f / 200f / 500f..." value="{{$package->price}}" >
                  </div>
                  <div class="form-group">
                    <label>Cyber</label>
                    <select class="form-control select2 select2-hidden-accessible" style="width: 100%;"  tabindex="-1" aria-hidden="true" name="cyber_id">
                      @foreach($cybers as $k => $v)
                        <option value="{{$v->id}}" {!! $package->cyber_id  == $v->id ? "selected" : "";!!} >{{$v->name}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Description</label>
                    <textarea class="form-control" rows="3" placeholder="Entrer la Description ..." name="description">{{$package->description}}</textarea>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Modifier</button>
                </div>
              </form>
            </div>

          </div>
        </div>
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
@endsection

