@extends('layouts.admin.admin') 

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Cybers</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('cyber.index')}}">Cyber</a></li>
              <li class="breadcrumb-item active">Index</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
        <form method="post" action="{{route('filter.cyber')}}">
          {{csrf_field()}}
          <div class="row">
            <div class="col-sm-2">
              <input type="text" name="search"  placeholder="Rechercher" value="" class="form-control">
            </div>
            <div class="col-sm-2">
              <select class="form-control select2 select2-hidden-accessible" style="width: 100%;"  tabindex="-1" aria-hidden="true" name="package_id">
                {{-- @foreach($packages as $k => $v)
                  <option value="{{$v->id}}">{{$v->name}}</option>
                @endforeach --}}
                <option value="">Choisir une option</option>
                <option value="">Option  1</option>
                <option value="">Option  2</option>
              </select>
            </div>
            <div class="col-sm-2">
              <button type="submit" class="btn btn-success btn-md mb-2">Valider</button>
            </div>
          </div>
        </form>
      </div>
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
            <a type="button" class="btn btn-primary btn-lg float-sm-right mb-2" href="{{route('cyber.create')}}">Ajouter un cyber</a>
          </div>

          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Listes de vos cybers</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">Id</th>
                      <th>Nom</th>
                      <th>Interface</th>
                      <th>Token</th>
                      <th style="width: 40px">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($cybers as $k => $v)
                    <tr>
                      <td>{{$k+1}}</td>
                      <td>{{$v->name}}</td>
                      <td>{{$v->gateway}}</td>
                      <td>{{$v->token}}</td>
                      <td>
                        <a class="btn btn-primary" href="{{route('cyber.edit',$v)}}">edit</a>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                  <li class="page-item"><a class="page-link" href="#">«</a></li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">»</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
@endsection

