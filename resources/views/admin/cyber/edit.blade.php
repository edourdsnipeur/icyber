@extends('layouts.admin.admin')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Ajouter un cyber</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('cyber.index')}}">Cyber</a></li>
              <li class="breadcrumb-item active">Ajouter un cyber</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <!-- /.card-header -->
              <!-- form start -->
              <form method="post" action="{{ route('cyber.update', $cyber) }}">
                {{csrf_field()}}
                <input type="hidden" name="_method" value="PUT">
                <div class="card-body">
                  <div class="form-group">
                    <label for="cybername">Nom du cyber</label>
                    <input type="text" class="form-control" name="cybername" id="cybername" placeholder="Nom du cyber" value="{{$cyber->name}}">
                  </div>
                  <div class="form-group">
                    <label for="ipayment">Interface de payement</label>
                    <input type="text" class="form-control" name="ipayment" id="ipayment" placeholder="ex: Paygate global" value="{{$cyber->gateway}}">
                  </div>
                  <div class="form-group">
                    <label for="token">Token</label>
                    <input type="text" class="form-control" name="token" id="token" placeholder="Token" value="{{$cyber->token}}">
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Modifer</button>
                </div>
              </form>
            </div>
            <!-- /.card -->

          </div>
        </div>
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
@endsection

