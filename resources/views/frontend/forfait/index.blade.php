@extends('layouts.frontend.frontend')

@section('content')
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-5" style="margin: 60px auto;">
          @if($ticket->status == 1)
          <div class="card card-success" style="text-align: center;">
              <div class="card-header">
                <h3 class="card-title">Votre Ticket</h3>
              </div>
              <div class="card-body">
                <code>Merci de copier votre login et votre mot de passe</code>
                <div class="form-group row mt-3">
                  <label for="username" class="col-3 col-form-label">Username</label>
                  <div class="col-7">
                    <input class="form-control form-control-md" value="{{$ticket->name}}" id="username" type="text" placeholder="username" name="username">
                  </div>
                  <div class="col-2">
                    <button type="button" class="btn btn-block btn-secondary btn-md" onclick="copie('username')">Copier</button>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="password" class="col-3 col-form-label">Password</label>
                  <div class="col-7">
                    <input class="form-control form-control-md" value="{{$ticket->password}}" id="password" type="text" placeholder="password" name="password"> 
                  </div>
                  <div class="col-2">
                    <button type="button" class="btn btn-block btn-secondary btn-md" onclick="copie('password')">Copier</button>
                  </div>
                </div>
                <button type="submit" class="btn btn-primary">Se connecter</button>
                         
                <hr>
                <div class="text-center pt-2 pb-2" style="color: gray;">
                    <p>
                        <strong>Vous pouvez toujours nous contacter pour toutes vos questions, remarques et réclamations, nous vous aideront avec grand plaisir.</strong>
                        <br>
                        (31 RUE DES DEUX EGLISES FACE A LA GRANDE MOSQUE 92104312 / 98197392)
                    </p>
                </div>
              </div>
              <!-- /.card-body -->
          </div>
          @elseif($ticket->status == 2)
          <div class="card card-warning" style="text-align: center;">
            <div class="card-header">
              <h3 class="card-title">Ticket en entente de payement</h3>
            </div>
            <div class="card-body">
              <code>Veuillez actualiser cette une fois que vous avez confirmer le payement.</code>
              
              <hr>
              <div class="text-center pt-2 pb-2" style="color: gray;">
                  <p>
                      <strong>Vous pouvez toujours nous contacter pour toutes vos questions, remarques et réclamations, nous vous aideront avec grand plaisir.</strong>
                      <br>
                      (31 RUE DES DEUX EGLISES FACE A LA GRANDE MOSQUE 92104312 / 98197392)
                  </p>
              </div>
            </div>


            <!-- /.card-body -->
          </div>
          @else
          <div class="card card-danger" style="text-align: center;">
            <div class="card-header">
              <h3 class="card-title">Payement annulé</h3>
            </div>
            <div class="card-body">
              <code>Vous avez annulé votre demande.</code>
              
              <hr>
              <div class="text-center pt-2 pb-2" style="color: gray;">
                  <p>
                      <strong>Vous pouvez toujours nous contacter pour toutes vos questions, remarques et réclamations, nous vous aideront avec grand plaisir.</strong>
                      <br>
                      (31 RUE DES DEUX EGLISES FACE A LA GRANDE MOSQUE 92104312 / 98197392)
                  </p>
              </div>
            </div>


            <!-- /.card-body -->
          </div>
          @endif
        </div>
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
            
@endsection

@push('js')
<script>
function copie(id) {
  /* Get the text field */
  var copyText = document.getElementById(id);
  copyText.select();
  copyText.setSelectionRange(0, 99999); /* For mobile devices */
  document.execCommand("copy");
}
</script>

@endpush