@extends('layouts.frontend.frontend')

@section('content')
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-5" style="margin: 60px auto;">
          
          <div class="card card-danger" style="text-align: center;">
            <div class="card-header">
              <h3 class="card-title">Ticket corrompu</h3>
            </div>
            <div class="card-body">
              <code>Veuillez recommencer le processus.</code>
              
              <hr>
              <div class="text-center pt-2 pb-2" style="color: gray;">
                  <p>
                      <strong>Vous pouvez toujours nous contacter pour toutes vos questions, remarques et réclamations, nous vous aideront avec grand plaisir.</strong>
                      <br>
                      (31 RUE DES DEUX EGLISES FACE A LA GRANDE MOSQUE 92104312 / 98197392)
                  </p>
              </div>
            </div>


            <!-- /.card-body -->
          </div>
          
        </div>
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
            
@endsection
