
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin | Mon WIFI-ZONE</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="/css/app.css">

  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/admin-lte@3.1/dist/css/adminlte.min.css">
</head>


<body class="hold-transition sidebar-mini">
<div class="wrapper" id="app">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="{{route('home')}}" class="nav-link">Home</a>
      </li>
    </ul>

  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="" class="brand-link">
      <img src="/images/ajalon-logo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">ETS Ajalon</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="/images/pp.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="{{route('home')}}" class="d-block">Emmanuel Fawuye</a>
        </div>
      </div>

     
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="{{route('home')}}" class="nav-link {{Request::is('home')?'active':''}}">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Tableau de bord
                <span class="right badge badge-danger">Nouveau</span>
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{route('cyber.index')}}" class="nav-link {{Request::is('cyber*')?'active':''}}">
              <i class="nav-icon fas fa-bars"></i>
              <p>Cyber</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{route('package.index')}}" class="nav-link {{Request::is('package*')?'active':''}}">
              <i class="nav-icon fas fa-bars"></i>
              <p>Forfait</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{route('ticket.index')}}" class="nav-link {{Request::is('ticket*')?'active':''}}">
              <i class="nav-icon fas fa-bars"></i>
              <p>Ticket</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{route('vente.index')}}" class="nav-link {{Request::is('vente*')?'active':''}}">
              <i class="nav-icon fas fa-bars"></i>
              <p>Vente</p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{route('client.index')}}" class="nav-link {{Request::is('client*')?'active':''}}">
              <i class="nav-icon fas fa-bars"></i>
              <p>Clients</p>
            </a>
          </li>
          

          <li class="nav-item">
            <a href="{{route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="nav-link">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p>
                Se Deconnecter
              </p>
            </a>
          </li>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{csrf_field()}}
            </form>
        
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  
  <!-- /.content-wrapper -->
  @yield('content')
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2014-{{date('Y')}} <a href="https://twitter.com/zinnoussou/with_replies">Edouard S. ZINNOUSSOU </a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 3.1.0-rc
    </div>
  </footer>
</div>
<!-- ./wrapper -->




<script src="/js/app.js"></script>
<script src="https://cdn.jsdelivr.net/npm/admin-lte@3.1/dist/js/adminlte.min.js"></script>
@stack('js')

</body>
</html>
