<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('root');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');




// Route::get('/vente', [App\Http\Controllers\VenteController::class, 'index'])->name('vente');
// Route::get('/fetchvente', [App\Http\Controllers\VenteController::class, 'fetch'])->name('fetchvente');
// Route::get('/searchvente/{q}', [App\Http\Controllers\VenteController::class, 'search'])->name('searchvente');
// Route::get('/searchventebyid/{id}', [App\Http\Controllers\VenteController::class, 'searchventebyid'])->name('searchventebyid');
// Route::post('/postvente', [App\Http\Controllers\VenteController::class, 'store'])->name('postvente');
// Route::put('/updatevente/{id}', [App\Http\Controllers\VenteController::class, 'update'])->name('updatevente');
// Route::delete('/deletevente/{id}', [App\Http\Controllers\VenteController::class, 'destroy'])->name('deletevente');
// Route::get('/query/{date}/{circle}', [App\Http\Controllers\HomeController::class, 'query'])->name('query');

///////////////
Route::post('paycallback', [App\Http\Controllers\TransactionController::class,'paycallback'])->name('paycallback');
Route::get('forfaitwelcome', [App\Http\Controllers\TransactionController::class,'forfaitwelcome'])->name('forfaitwelcome');
Route::post('filter/ticket', [App\Http\Controllers\TicketController::class,'filter'])->name('filter.ticket');
Route::resource('cyber', App\Http\Controllers\CyberController::class);
Route::resource('package', App\Http\Controllers\PackageController::class);
Route::resource('ticket', App\Http\Controllers\TicketController::class);
Route::post('ticket/importcsv', [App\Http\Controllers\TicketController::class,'importcsv'])->name('ticket.importcsv');
Route::post('filter/cyber', [App\Http\Controllers\CyberController::class,'filter'])->name('filter.cyber');
Route::resource('vente', App\Http\Controllers\VenteController::class);

// Route::group(['as'=>'admin.','prefix'=>'admin', 'middleware'=>['admin'], 'namespace'=>'Admin'],function (){
// 	}
// );
Route::group(['as'=>'api.', 'prefix'=>'api/v1'], function (){
	Route::get('forfait', [App\Http\Controllers\TransactionController::class,'transtionbuy'])->name('transtionbuy');
	}
);

Route::resource('client', App\Http\Controllers\ClientController::class);
