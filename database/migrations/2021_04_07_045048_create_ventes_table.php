<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVentesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ventes', function (Blueprint $table) {
            $table->id();
            $table->integer('package_id')->unsigned();
            $table->integer('ticket_id')->unsigned();
            $table->integer('cyber_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('price');
            $table->string('tx_reference')->default(null);
            $table->string('identifier')->unique();
            $table->string('payment_reference')->default(null);
            $table->string('datetime')->default(null);
            $table->string('payment_method')->default(null);
            // 0 : Paiement réussi avec succès 2 : En cours 4 : Expiré 6: Annulé
            $table->integer('status')->default(2);
            $table->string('phone_number')->default(null);
            $table->timestamps();
            $table->foreign('package_id')->references('id')->on('packages')->onDelete('cascade');
            $table->foreign('ticket_id')->references('id')->on('tickets')->onDelete('cascade');
            $table->foreign('cyber_id')->references('id')->on('cybers')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ventes');
    }
}
