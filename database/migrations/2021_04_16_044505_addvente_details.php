<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddventeDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ventes', function (Blueprint $table) {
            $table->string('tx_reference')->nullable();
            $table->string('identifier')->unique();
            $table->string('payment_reference')->nullable();
            $table->string('datetime')->nullable();
            $table->string('payment_method')->nullable();
            // 0 : Paiement réussi avec succès 2 : En cours 4 : Expiré 6: Annulé
            $table->integer('status')->default(2);
            $table->string('phone_number')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ventes', function (Blueprint $table) {
            $table->dropColumn('tx_reference');
            $table->dropColumn('identifier');
            $table->dropColumn('payment_reference');
            $table->dropColumn('datetime');
            $table->dropColumn('payment_method');
            $table->dropColumn('status');
            $table->dropColumn('phone_number');
        });
    }
}
