<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'=> 'Admin',
            'email'=>'admin@triooti.com',
            'password'=>bcrypt('admin')
        ]);
        DB::table('users')->insert([
            'name'=> 'Sky',
            'email'=>'sky@triooti.com',
            'password'=>bcrypt('sky01')
        ]);
    }
}
