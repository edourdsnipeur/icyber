<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cyber extends Model
{
    use HasFactory;

    public function ventes()
    {
        return $this->hasMany(Ventes::class);
    }
    public function packages()
    {
        return $this->hasMany(Package::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
