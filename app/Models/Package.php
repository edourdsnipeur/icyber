<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    use HasFactory;
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function cyber()
    {
        return $this->belongsTo(Cyber::class);
    }
    public function ventes()
    {
        return $this->hasMany(Vente::class);
    }
    public function tickets()
    {
        return $this->hasMany(Ticket::class);
    }
}
