<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vente extends Model
{
    use HasFactory;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function package()
    {
        return $this->belongsTo(Package::class);
    }
    public function cyber()
    {
        return $this->belongsTo(Cyber::class);
    }

    public function ticket()
    {
        return $this->belongsTo(Ticket::class);
    }
    public function client()
    {
        return $this->belongsTo(Client::class);
    }
}
