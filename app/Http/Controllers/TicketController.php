<?php

namespace App\Http\Controllers;

use App\Models\Ticket;
use App\Models\Package;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class TicketController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // faire un orderBy
        $packages = Auth::user()->packages()->get()->all();
        $tickets = Auth::user()->tickets()->paginate(10);
        return view('admin.ticket.index',compact(['packages', 'tickets']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.ticket.create');
    }

    public function importcsv(Request $request)
    {
        $this->validate($request,[
        'package_id'=> 'required',
        // 'csvFile'=>'mimes',
        ]);

        $file = $request->file('csvFile');
        $content = fopen($file,'r');

        $idIndex = $request->id;
        $paswordIndex = $request->password;
        $user=Auth::user();
        $ticketPice = Package::find($request->package_id)->price;

        while(($line = fgetcsv($content)) !== false){
        $ticketName = $line[$idIndex-1];
        $ticketPass = $line[$paswordIndex-1];
            if ($ticketName !== null && $ticketName !== '') {
                $ticket = new Ticket();
                $ticket->user_id = $user->id;
                $ticket->package_id = $request->package_id;
                $ticket->name = $ticketName;
                $ticket->price = $ticketPice;
                // $ticket->password = $ticketPass;
                $ticket->save();
            }
        }
        fclose($content);
        
        return redirect()->route('ticket.index');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function show(Ticket $ticket)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function edit(Ticket $ticket)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ticket $ticket)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ticket $ticket)
    {
        //
    }

    public function filter(Request $request)
    {
        // dd($request->all());
        $tickets=Auth::user()->tickets();
        if (!is_null($request->status)) {
            $tickets= $tickets->where('status', $request->status);
        }
        if (!is_null($request->search)) {
            
        }
        $tickets= $tickets->paginate(10);
        $packages = Auth::user()->packages()->get()->all();
        return view('admin.ticket.index',compact(['packages', 'tickets']));

    }
}
