<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Ticket;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $packages = Auth::user()->packages()->get()->all();
        $tickets = Auth::user()->tickets()->get();
        $dashboardPackage = [];
        foreach ($packages as $package) {
            $packageXtickets = $tickets->filter(function ($ticket) use($package) {return $ticket->package_id == $package->id;});
            $packageSell = $packageXtickets->filter(function ($ticket) {return $ticket->status == 1;});
            $packageEntente = $packageXtickets->filter(function ($ticket) {return $ticket->status == 2;});
            array_push($dashboardPackage, [$package->name, $packageXtickets->count(),  $packageSell->count(), $packageEntente->count()]);
        }
        //Calcule des ventes de ce mois et preceeedent
        $d = date_create(date('j-m-Y'));
        // $d = date_create('01-07-2019');
        $d->modify('first day of this month');
        $beginDateThis = $d->format('Y-m-j');
        $d->modify('last day of this month');
        $endDateThis = $d->format('Y-m-j');
        $sommeThis = Auth::user()->ventes()->where("created_at", '>=', $beginDateThis)->where("created_at", '<=', $endDateThis)->where("status", 0)->get('price')->sum->price;

        $d->modify('first day of last month');
        $beginDatePrec = $d->format('Y-m-j');
        $d->modify('last day of this month');
        $endDatePrec = $d->format('Y-m-j');
        $sommePrec = Auth::user()->ventes()->where("created_at", '>=', $beginDatePrec)->where("created_at", '<=', $endDatePrec)->where("status", 0)->get('price')->sum->price;
        //dd($sommeThis);
        Log::info('mon home.');
        return view('admin.home', compact(['dashboardPackage','sommeThis','sommePrec']));
    }
}
