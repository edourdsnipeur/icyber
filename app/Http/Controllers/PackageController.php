<?php

namespace App\Http\Controllers;

use App\Models\Package;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PackageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $packages = Auth::user()->packages()->with('cyber')->get()->all();
        return view('admin.package.index',compact(['packages']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cybers = Auth::user()->cybers()->get()->all();
        return view('admin.package.create',compact(['cybers']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd();
        $this->validate($request,[
            'packagename' => 'required',
            'price' => 'required',
            'description' => 'required'
        ]);
        $user=Auth::user();
        $package = new Package();
        $package->name = $request->packagename;
        $package->price = $request->price;
        $package->token = uniqid('sky290');
        $package->description = $request->description;
        $package->user_id = $user->id;
        $package->cyber_id = $request->cyber_id;

        $package->save();
        return redirect()->route('package.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function show(Package $package)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function edit(Package $package)
    {
        $cybers = Auth::user()->cybers()->get()->all();
        return view('admin.package.edit',compact(['package','cybers']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Package $package)
    {
        $this->validate($request,[
            'packagename' => 'required',
            'price' => 'required',
            'description' => 'required'
        ]);
        
        $package->name = $request->packagename;
        $package->price = $request->price;
        $package->description = $request->description;
        $package->cyber_id = $request->cyber_id;

        $package->update();
        return redirect()->route('package.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function destroy(Package $package)
    {
        //
    }
}
