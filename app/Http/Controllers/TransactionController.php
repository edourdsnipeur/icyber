<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Package;
use App\Models\Ticket;
use App\Models\Vente;
use App\Models\Client;
use Illuminate\Support\Facades\Http;

class TransactionController extends Controller
{
    
    public function transtionbuy(Request $request)
    {
    	$this->validate($request,[
            'token' => 'required',
        ]);
        $pack = Package::where('token', $request->token)->with('cyber')->first();
        if (!is_null($pack)) {
        	// status === 2 => ticket en entente de commandes
        	$ticket = Ticket::where(['package_id'=> $pack->id, 'status'=> 0])->first();
        	if ($ticket) {
        		$identifier = uniqid('sky');
        		$ticket->status = 2;
	        	$ticket->update();
	        	$vente = new Vente();
	        	$vente->package_id = $pack->id;
	        	$vente->ticket_id = $ticket->id;
	        	$vente->cyber_id = $pack->cyber_id;
	        	$vente->user_id = $pack->user_id;
	        	$vente->price = $pack->price;
	        	$vente->identifier = $identifier;
	        	$vente->save();
	        	return redirect()->to("https://paygateglobal.com/v1/page?token=".$pack->cyber->token."&amount=".$pack->price."&description=".$pack->description."&identifier=".$identifier."&url=".route("forfaitwelcome",['identifier'=> $identifier]));
        	}
        	return  'ticket fini';
        }
        return  'faux forfait ';
    }

    public function paycallback(Request $request)
    {
    	$data = $request->json()->all();
        $vente = Vente::where('identifier', $data['identifier'])->first();

        $client = Client::where('user_id', $vente->user_id)->where('phone', $data['phone_number'])->first();
        if (!$client) {
            $client = new Client();
            $client->user_id = $vente->user_id;
            $client->phone = $data['phone_number'];
            $client->save();
        }

        $vente->tx_reference = $data['tx_reference'];
        $vente->payment_reference = $data['payment_reference'];
        $vente->datetime = $data['datetime'];
        $vente->payment_method = $data['payment_method'];
        $vente->phone_number = $data['phone_number'];
        $vente->update();  
        return  response(['data'=>'success']);

    }
	public function forfaitwelcome(Request $request)
    {
        if (!is_null($request->identifier)) {
         	$vente = Vente::where('identifier', $request->identifier)->with('cyber')->first();
         	if ($vente) {
         		$ticket = Ticket::find($vente->ticket_id);
			    $myBody['auth_token'] = $vente->cyber->token;
			    $myBody['identifier'] = $request->identifier;
         		
         		$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL,"https://paygateglobal.com/api/v2/status");
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($myBody));
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				$response = json_decode(curl_exec($ch)) ;
				curl_close ($ch);

         		// dd($response);
         		if (!is_null($response)) {
         			if ($response->status == 0) {
	         			$ticket->status = 1;
	         			$ticket->update();
	         		}elseif ($response->status == 6 || $response->status == 4) {
	         			$ticket->status = 0;
	         			$ticket->update();
	         		}elseif ($response->status == 2) {
	         			$ticket->status = 2;
	         			$ticket->update();
	         		}
	         		$vente->status = $response->status;
	         		$vente->tx_reference = $response->tx_reference;
	         		$vente->payment_reference	 = $response->payment_reference;
	         		$vente->datetime = $response->datetime;
	         		$vente->payment_method = $response->payment_method;
	         		$vente->update();
         		}
         		
         		return view('frontend.forfait.index',compact(['ticket']));
         	}

        } 
        return view('frontend.forfait.error');
    }

}
