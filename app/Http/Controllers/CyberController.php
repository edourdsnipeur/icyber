<?php

namespace App\Http\Controllers;

use App\Models\Cyber;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class CyberController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cybers = Auth::user()->cybers()->get()->all();
        // dd($cybers);
        return view('admin.cyber.index',compact(['cybers']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.cyber.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $this->validate($request,[
            'cybername' => 'required',
            'ipayment' => 'required',
            'token' => 'required'
        ]);
        $user=Auth::user();
        $cyber = new Cyber();
        $cyber->name = $request->cybername;
        $cyber->gateway = $request->ipayment;
        $cyber->token = $request->token;
        $cyber->user_id = $user->id;

        $cyber->save();
        return redirect()->route('cyber.index');


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Cyber  $cyber
     * @return \Illuminate\Http\Response
     */
    public function show(Cyber $cyber)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Cyber  $cyber
     * @return \Illuminate\Http\Response
     */
    public function edit(Cyber $cyber)
    {
        return view('admin.cyber.edit',compact(['cyber']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Cyber  $cyber
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cyber $cyber)
    {
        $this->validate($request,[
            'cybername' => 'required',
            'ipayment' => 'required',
            'token' => 'required'
        ]);
        $cyber->name = $request->cybername;
        $cyber->gateway = $request->ipayment;
        $cyber->token = $request->token;
    
        $cyber->update();
        return redirect()->route('cyber.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Cyber  $cyber
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cyber $cyber)
    {
        //
    }


    public function filter(Request $request)
    {
        // dd($request->all());
        $this->validate($request,[
            'search' => 'required',
        ]);
        $cybers=Auth::user()->cybers->where('name', $request->search);
        dd($cybers);
        return redirect()->route('cyber.index');


    }
}
