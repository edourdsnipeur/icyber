<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Vente;
use App\Models\Ticket;
class EraseSuspendVente extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ventes:erasesuspended';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Supprime tous les ventes qui sont echouer';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //supprime les vente echouer et expiré et les ventes lancer dans plus de 5minuteeeee
        Vente::where('status', 4)->delete();
        Vente::where('status', 6)->delete();
        Vente::where('status', 2)->where('updated_at', '<=', date("Y-m-d H:i:s",strtotime("-5 minutes")))->delete();
        //Revalide les ticket non vendu finalement 
        Ticket::where('status', 2)->where('updated_at', '<=', date("Y-m-d H:i:s",strtotime("-5 minutes")))->update(['status'=> 0]);

        $this->info('Systèmes mise à niveau avec succès');
        return 0;
    }
}
